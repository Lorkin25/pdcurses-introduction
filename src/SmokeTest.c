
#include <stdlib.h> // Include stdlib.h for exit code constants
#include <curses.h> // Include curses library header to be able to use PDCurses

// Define array of pointers to strings with ASCII art to display
// (each array element point to one row of characters)
const char* artwork[] = {
    " _   _      _ _          _    _            _     _ _ ",
    "| | | |    | | |        | |  | |          | |   | | |",
    "| |_| | ___| | | ___    | |  | | ___  _ __| | __| | |",
    "|  _  |/ _ \\ | |/ _ \\   | |/\\| |/ _ \\| '__| |/ _` | |",
    "| | | |  __/ | | (_) |  \\  /\\  / (_) | |  | | (_| |_|",
    "\\_| |_/\\___|_|_|\\___( )  \\/  \\/ \\___/|_|  |_|\\__,_(_)",
    "                    |/                               "
};

// Define some important constant values
const int artWidth = 54;    // Width (in chars) of the artwork
const int artHeight = 7;    // Height (in rows) of the artwork
const int marginVer = 1;    // Margin (in rows) above and below the artwork
const int marginHor = 5;    // Margin (in chars) on both sides of the artwork

// The main (and the only) function in this program
int main() {

    // Prepare PDCurses system
    initscr();              // Start curses engine and erase main window
    start_color();          // Turn on color mode
    curs_set(0);            // Hide the text cursor
    refresh();              // Actually show the main window on the screen

    // Calculate dimensions and top-left coordinates for banner window
    // centered on the screen
    int bannerWidth = artWidth + 2*marginHor;
    int bannerHeight = artHeight + 2*marginVer;
    int bannerTop = (LINES - bannerHeight)/2;
    int bannerLeft = (COLS - bannerWidth)/2;

    // Create banner window
    WINDOW* banner = newwin(bannerHeight, bannerWidth, bannerTop, bannerLeft);
    if (!banner) {                  // If window creation goes wrong
        endwin();                   // Shut down curses
        return EXIT_FAILURE;        // Report failure to the OS
    }

    // Prepare two color pairs used for painting banner
    init_pair(1, COLOR_WHITE, COLOR_BLUE);  // White chars on blue background
    init_pair(2, COLOR_RED, COLOR_RED);     // Red chars on red background

    // Prepare banner background
    wbkgd(banner, COLOR_PAIR(1));       // Fill background with blue color
    wattrset(banner, COLOR_PAIR(2));    // Set red color for drawing
    box(banner, ' ', ' ');              // Draw a borded with red spaces
    wrefresh(banner);                   // Display prepared banner

    // Draw the artwork on the banner window
    wattrset(banner, COLOR_PAIR(1));    // Set white chars on blue background
    int i;
    for (i = 0; i < artHeight; i++) {   // Iterate through all rows
        napms(200);                     // Wait for 0.2 s
        // Print next row from artwork to banner window
        mvwaddstr(banner, marginVer+i, marginHor, artwork[i]);
        wrefresh(banner);       // Update banner window view on the screen
    }

    // Draw timeout bar on the main window
    for (i = 1; i < COLS-1; i++) {      // Iterate through all columns
        mvaddch(LINES-1, i, '|');       // Print | in i-th column of last row
        refresh();                      // Update main window view
        napms(30);                      // Delay 0.03 s
    }

    // Cleanup
    delwin(banner);         // Delete banner window and free its resources
    endwin();               // Shut down curses and restore terminal
    return EXIT_SUCCESS;    // Exit with success code to the OS
}

#include <stdlib.h>
#include <curses.h>

// Constant symbols
enum {
    BASIC_COLORS = 6,                   // Number of pure colors in palette
    PALETTE_SIZE = 2*BASIC_COLORS + 2,  // Pure + mixed colors + black & white
    IFACE_PAIR = 1,                     // Color pair for interface drawing
    PALETTE_WIDTH = 7                   // Width of palette window
};

// Window handles
WINDOW* canvasWnd;
WINDOW* paletteWnd;

// Palette of inks
chtype palette[PALETTE_SIZE];       // Ink = character + color pair + attribute
int currentInk = PALETTE_SIZE-2;    // Start with white ink

// Function headers
void preparePalette();
void processMouseWheel();
void processCanvasMouse(int x, int y);
void processPaletteMouse(int x, int y);
void paintPalette();

int main() {
    initscr();
    start_color();
    curs_set(0);
    keypad(stdscr, TRUE);        // Keypad mode must be on to receive KEY_MOUSE
    mouse_on(ALL_MOUSE_EVENTS);  // Turn on tracking of all mouse events
    refresh();

    paletteWnd = newwin(PALETTE_SIZE+2, PALETTE_WIDTH, 0, 0);
    canvasWnd = newwin(LINES, COLS-PALETTE_WIDTH, 0, PALETTE_WIDTH);
    if (!paletteWnd || !canvasWnd) {
        endwin();
        return EXIT_FAILURE;
    }

    preparePalette();
    paintPalette();

    int running = TRUE;
    while (running) {
        int key = getch();
        switch (key) {
            case KEY_MOUSE:
                request_mouse_pos();    // Ask PDCurses to retrieve mouse status
                // Check if mouse wheel event occured
                if (MOUSE_WHEEL_UP || MOUSE_WHEEL_DOWN) {
                    processMouseWheel();
                    break;
                }
                int x, y;
                // Get mouse coordinates relative to canvasWnd
                wmouse_position(canvasWnd, &y, &x);
                // (x and y become negative if mouse is outside window)
                if (x > 0) {     // If mouse in within canvas window
                    processCanvasMouse(x, y);
                    break;
                }
                // Otherwise check palette window analogously
                wmouse_position(paletteWnd, &y, &x);
                if (x > 0) {
                    processPaletteMouse(x, y);
                    break;
                }
                break;
            case 033:   // ESCAPE
                running = FALSE;
                break;
            /* TODO: Add clear canvas feature */
            /* TODO: Add image saving and loading feature */
        }
    }

    delwin(canvasWnd);
    delwin(paletteWnd);
    endwin();
    return EXIT_SUCCESS;
}

/**
 * @brief Initialize color pairs and fill palette with ready to paint inks
 */
void preparePalette() {
    // Prepare color pair (white on black) for drawing interface elements
    init_pair(IFACE_PAIR, COLOR_WHITE, COLOR_BLACK);

    int pair = IFACE_PAIR;
    // pair will be preincremented before registering next pairs
    short colors[] = { COLOR_RED, COLOR_YELLOW, COLOR_GREEN, COLOR_CYAN,
                       COLOR_BLUE, COLOR_MAGENTA, COLOR_RED };
                       // RED is repeated twice for consistent iteration
    int b;
    // Generate rainbow inks
    for (b = 0; b < BASIC_COLORS; b++) {
        // Solid color ink
        init_pair(++pair, colors[b], colors[b]);
        palette[2*b] = ACS_BLOCK | COLOR_PAIR(pair) | A_BOLD;
        // Mixed checkerboard ink
        init_pair(++pair, colors[b], colors[b+1]);
        palette[2*b+1] = ACS_CKBOARD | COLOR_PAIR(pair) | A_BOLD;
    }

    // Add white and black inks
    init_pair(++pair, COLOR_WHITE, COLOR_WHITE);
    palette[PALETTE_SIZE-2] = ACS_BLOCK | COLOR_PAIR(pair) | A_BOLD;
    init_pair(++pair, COLOR_BLACK, COLOR_BLACK);
    palette[PALETTE_SIZE-1] = ACS_BLOCK | COLOR_PAIR(pair) | A_NORMAL;
}

/**
 * @brief Draw palette window on the screen
 */
void paintPalette() {
    wattrset(paletteWnd, COLOR_PAIR(IFACE_PAIR) | A_NORMAL);
    wclear(paletteWnd);
    box(paletteWnd, 0, 0);
    mvwaddch(paletteWnd, 1+currentInk, 1, '>' | A_BOLD);
    mvwaddch(paletteWnd, 1+currentInk, PALETTE_WIDTH-2, '<' | A_BOLD);
    int ink, col;
    for (ink = 0; ink < PALETTE_SIZE; ink++) {
        for (col = 2; col < PALETTE_WIDTH-2; col++) {
            mvwaddch(paletteWnd, 1+ink, col, palette[ink]);
        }
    }
    wrefresh(paletteWnd);
}

/**
 * @brief Process mouse event targeted at canvas window
 * @param x,y  Mouse coordinates relative to canvas window
 */
void processCanvasMouse(int x, int y) {
    // Process events associated with left mouse button
    if (BUTTON_CHANGED(1)) {
        // Get button action code from button status
        short buttonAction = BUTTON_STATUS(1) & BUTTON_ACTION_MASK;
        switch (buttonAction) {
            case BUTTON_PRESSED:          // Button pressed down and held
            case BUTTON_CLICKED:          // Pressed and released quickly
            case BUTTON_DOUBLE_CLICKED:   // Clicked twice quickly
                mvwaddch(canvasWnd, y, x, palette[currentInk]);
                break;

            case BUTTON_RELEASED:   // Released after being held for a while
                break;

            case BUTTON_MOVED:      // Mouse draged with button pressed
                mvwaddch(canvasWnd, y, x, palette[currentInk]);
                /* TODO: Draw continuous line segment from last mouse position
                 * instead of single pixel
                 */
                break;
        }
        wrefresh(canvasWnd);
    }

    // Process right mouse button events
    if (BUTTON_CHANGED(2)) {
        /* TODO: Add erasing feature for right mouse button
         */
    }
}

/**
 * @brief Process mouse event targeted at palette window
 * @param x,y  Mouse coordinates relative to palette window
 */
void processPaletteMouse(int x, int y) {
    // Only clicks on ink swatches are processed
    if (y < 1 || y > PALETTE_SIZE) {
        return;
    }
    currentInk = y-1;
    paintPalette();
}

/**
 * @brief Process mouse wheel event (regardless of mouse position)
 */
void processMouseWheel() {
    if (MOUSE_WHEEL_DOWN && currentInk < PALETTE_SIZE-1) {
        currentInk++;
        paintPalette();
    }
    if (MOUSE_WHEEL_UP && currentInk > 0) {
        currentInk--;
        paintPalette();
    }
}
